FROM nginx:1.19.2-alpine

COPY default.conf.template /etc/nginx/conf.d/default.conf.template

COPY run.sh /
ENTRYPOINT ["/run.sh"]
CMD ["nginx", "-g", "daemon off;"]