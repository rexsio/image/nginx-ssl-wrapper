# Nginx SSL wrapper

## Environment Variables
Name | Description
--- | ---
LISTEN_PORT | Defines nginx listen port
API_URL | Defines reveres proxy target URL

## Certificates
### Mandatory Config
* Define a certificate under `/etc/nginx/certificates/server.crt`.
* Define a certificate private key under `/etc/nginx/certificates/private.key`

### Optional Config
* Define certificates chain under `/etc/nginx/certificates/ca-certificates.chain` if you want nginx to serve the entire certificate structure

