#!/usr/bin/env sh
set -eu

SERVER_CERT_PATH=/etc/nginx/certificates/server.crt
if [ -f "$SERVER_CERT_PATH" ]; then
  cp $SERVER_CERT_PATH /etc/nginx/certificates/full-server.crt
else
    echo "$SERVER_CERT_PATH does not exist. Please specify nginx server certificate."
fi

CHAIN_CERT_PATH=/etc/nginx/certificates/ca-certificates.chain
if [ -f "$CHAIN_CERT_PATH" ]; then
  sed -i -e '$a\' /etc/nginx/certificates/full-server.crt
  cat $CHAIN_CERT_PATH >> /etc/nginx/certificates/full-server.crt
fi

envsubst '${LISTEN_PORT} ${API_URL}' < /etc/nginx/conf.d/default.conf.template > /etc/nginx/conf.d/default.conf

exec "$@"